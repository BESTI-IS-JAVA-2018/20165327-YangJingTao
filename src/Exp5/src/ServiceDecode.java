import java.net.*;
import java.io.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import javax.crypto.interfaces.*;
import java.security.interfaces.*;
import java.math.*;

public class ServiceDecode {
    public static void main(String args[]) throws Exception {
        ServerSocket link = null;
        Socket socket = null;
        try {
            link = new ServerSocket(2018);// 创建服务器套接字
            System.out.println("端口号:" + link.getLocalPort());
            System.out.println("服务器已经启动...");
            socket = link.accept(); // 等待客户端连接
            System.out.println("已经建立连接");
            //获得网络输入流对象的引用
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //获得网络输出流对象的引用
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            // 使用服务器端RSA的私钥对DES的密钥进行解密

            String line = in.readLine();
            BigInteger cipher = new BigInteger(line);
            FileInputStream f = new FileInputStream("Skey_RSA_priv.dat");
            ObjectInputStream b = new ObjectInputStream(f);
            RSAPrivateKey prk = (RSAPrivateKey) b.readObject();
            BigInteger d = prk.getPrivateExponent();
            BigInteger n = prk.getModulus();//mod n
            BigInteger m = cipher.modPow(d, n);//m=d (mod n)
            System.out.println("d= " + d);
            System.out.println("n= " + n);
            System.out.println("m= " + m);
            byte[] keykb = m.toByteArray();
            // 使用DES对密文进行解密
            String readline = in.readLine();//读取客户端传送来的数据
            FileInputStream f2 = new FileInputStream("keykb1.dat");
            int num2 = f2.available();
            byte[] ctext = parseHexStr2Byte(readline);
            Key k = new SecretKeySpec(keykb,"DESede");
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            byte[] ptext = cp.doFinal(ctext);
            String p = new String(ptext, "UTF8");//编码转换
            System.out.println("从客户端接收到信息为：" + p); //打印解密结果
            // 使用Hash函数检测明文完整性
            String aline3 = in.readLine();
            String x = p;
            MessageDigest m2 = MessageDigest.getInstance("MD5");//使用MD5算法返回实现指定摘要算法的 MessageDigest对象
            m2.update(x.getBytes());
            byte a[] = m2.digest();
            String result = "";
            for (int i = 0; i < a.length; i++) {
                result += Integer.toHexString((0x000000ff & a[i]) | 0xffffff00).substring(6);
            }
            System.out.println(result);
            if (aline3.equals(result)) {
                System.out.println("匹配成功");
            }
            MyDC evaluator = new MyDC();
            int _result = evaluator.value(p);
            out.println("Echo:" + _result);
            out.close();
            in.close();
            link.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    //二进制转换成十六进制，防止byte[]数字转换成string类型时造成的数据损失
    public static String parseByte2HexStr(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());//将字符串中的小写字母转换成大写字母,然后加在字符串上
        }
        return sb.toString();
    }
    //将十六进制转换为二进制
    public static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1)
            return null;
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1),16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2),16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }
}