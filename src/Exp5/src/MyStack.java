interface SStack<T> {//栈接口，栈数据抽象类型
    boolean isEmpty();//判断是否空栈
    void push(T x);//元素x入栈
    T pop();//出栈，返回当前栈顶元素
    T get();//取栈顶元素
}
public class MyStack<T> implements SStack<T> {
    private Object element[];//储存数据数组
    private int top;//栈顶元素下标
    public MyStack(int size){//容纳大小为size的栈
        this.element = new Object[Math.abs(size)];
        this.top = -1;
    }
    public MyStack() {
        this(64);//栈大小为64
    }
    public boolean isEmpty() {
        return this.top == -1;//空栈栈顶元素为-1
    }
    public void push(T x) {//元素x入栈
        if(x==null)
            return;//栈空时不如栈
        if(this.top == element.length-1){//栈满时，申请更大空间
            Object[] temp = this.element;
            this.element = new Object[temp.length*2];
            for(int i = 0; i < temp.length; i++)
                this.element[i] = temp[i];
        }
        this.top++;
        this.element[this.top] = x;
    }
    public T pop() {
        return this.top==-1 ? null:(T)this.element[this.top--];//弹栈
    }
    public T get() {
        return this.top==-1 ? null:(T)this.element[this.top];//取元素
    }
}