import java.util.*;
public class MyList {
public static void main(String [] args) {
    List<String> mylist=new LinkedList<String>();
//选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
    mylist.add("20165325");
    mylist.add("20165326");
    mylist.add("20165328");
    mylist.add("20165329");
//把上面四个节点连成一个没有头结点的单链表
    System.out.println("打印初始链表");
//遍历单链表，打印每个结点的
    Iterator<String> iter=mylist.iterator();
    while (iter.hasNext()){
        String num=iter.next();
        System.out.println(num);
    }
//把你自己插入到合适的位置（学号升序）
    mylist.add("20165327");
    Collections.sort(mylist);
//遍历单链表，打印每个结点的
    System.out.println("插入我的学号在排序之后，链表中的数据：");
    iter =mylist.iterator();
    while(iter.hasNext()){
        String num=iter.next();
        System.out.println(num);
    }
//从链表中删除自己
    mylist.remove("20165327");
    System.out.println("删除我的学号之后打印链表：");
//遍历单链表，打印每个结点的
    iter=mylist.iterator();
    while(iter.hasNext()){
        String num=iter.next();
        System.out.println(num);
    }
}
}
