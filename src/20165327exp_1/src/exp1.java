/**
 * Created by yjt on 2018/4/2.
 */
import java.util.Scanner;
public class exp1 {
    public static void main(String[] args) {
        Encrypy p1 = new Encrypy();
        Decrypt p2 = new Decrypt();
        Scanner in = new Scanner(System.in);
        System.out.print("请选择操作（1:加密 2:解密）:");
        int n = in.nextInt();
        if(n == 1) {       //加密
            System.out.println("请输入明文：");
            String mingwen = in.next();
            System.out.println("请输入密钥：");
            int miyao = in.nextInt();
            try {
                String secret = p1.encrpy(mingwen,miyao);
                System.out.println("密文是："+secret);
            }
            catch(exp1Exception e) {
                System.out.println(e.warnMess());
            }
        }
        else if(n == 2) {    //解密
            System.out.println("请输入密文：");
            String miwen = in.next();
            System.out.println("请输入密钥：");
            int miyao = in.nextInt();
            try {
                String source = p2.decrypt(miwen,miyao);
                System.out.println("明文是："+source);
            }
            catch(exp1Exception e) {
                System.out.println(e.warnMess());
            }
        }
        else {    //异常情况
            System.out.println("请正确选择");
        }
    }
}

//加密算法

class Encrypy {
    String encrpy(String mingwen,int miyao) throws exp1Exception {
        char[] c = mingwen.toCharArray();
        int a = c.length;
        for(int i=0;i<a;i++) {
            if ((c[i] >= 'a' && c[i] <='z') || (c[i] >= 'A' && c[i] <= 'Z')) {     //判断是否为英文字母
                if(c[i] >= 'a' && c[i] <='z') {     //加密
                    c[i] = (char) ( (c[i] - 'a'+ miyao) % 26 + 'a');
                }
                else {
                    c[i] = (char) ( (c[i] - 'A'+ miyao) % 26 + 'A');
                }
            } else {
                throw new exp1Exception(mingwen);
            }
        }
        return new String(c);     //返回密文
    }
}

//解密算法

class Decrypt {
    String decrypt(String miwen,int miyao) throws exp1Exception {
        char[] c = miwen.toCharArray();
        int b = c.length;
        for(int i=0;i<b;i++) {
            if ((c[i] >= 'a' && c[i] <= 'z') || (c[i] >= 'A' && c[i] <= 'Z')) {     //判断是否为英文字母

                if(c[i] >= 'a' && c[i] <='z') {
                    int n = c[i] - 'a' - miyao;  //解密
                    if (n < 0) {
                        n = n + 26;
                    }
                    int mima = n % 26 + 'a';
                    c[i] = (char) mima;
                }
                else {
                    int m = c[i] - 'A' - miyao;
                    if (m < 0) {
                        m = m + 26;
                    }
                    int mima = m % 26 + 'A';
                    c[i] = (char) mima;
                }
            }
            else {
                throw new exp1Exception(miwen);
            }
        }
        return new String(c);   //返回明文
    }
}


//自定义异常

class exp1Exception extends  Exception {
    String message;
    public exp1Exception(String wen){
        message = "输入错误，发生异常";
    }
    public String warnMess(){
        return message;
    }

}
