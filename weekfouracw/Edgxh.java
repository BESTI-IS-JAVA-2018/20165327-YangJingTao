import java.util.Scanner;
class Edgxh
 {
   public static void main(String[] args) {
      System.out.println("输入n,求出1!+2!+3!+...+n!的值");
      Scanner in=new Scanner(System.in);
      int n=in.nextInt();
      while(n<1) {
         System.out.println("请正整数:");
         n=in.nextInt();
       }
      int sum=0;
      for(int i=1;i<=n;++i)
      sum+=recursion(i);
      System.out.println(sum);
}
   public static int recursion(int n){
      if(n==1)
      return 1;
      else
      return n*recursion(n-1);

   }
}